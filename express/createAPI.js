const express = require('express')
const fs = require('fs')
const cors = require('cors')
const path = require('path')
const bodyParser = require('body-parser');
const app = express()
const port = 8000


function readFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    })
  })
}
app.use(cors());
app.use(bodyParser.json());
app.get('/', (req, res) => {
  readFile(path.join(__dirname, '../products.json'))
    .then(data => {
      res.send(data);
    }, err => {
      console.log(err);
    });

})
app.get('/products/:id', (req, res) => {
  readFile(path.join(__dirname, '../products.json'))
    .then(data => {
      const value = JSON.parse(data);
      const getValue = value["response"]["data"].find((element, index) => req.params.id == index);
      res.send(getValue)
    }, err => {
      console.log(err);
    });
})

app.post('/order', async (req, res) => {
  let orderData = '[]';
  if(fs.existsSync('../public/Orders.json')) {
  orderData = await readFile(path.join(__dirname, '../public/Orders.json'));
  }

  const orderId = Math.floor(Math.random() * 10000000000) + 10000000000;
  if(req.body.length>0) {
  const data = JSON.stringify([...(JSON.parse(orderData)), { [orderId]: req.body }]);
    fs.writeFile(path.join(__dirname, '../public/Orders.json'), data, (err) => {
      if (err) {
        console.log(err)
      } else {
        console.log('saved Successfully')
      }
    })
  }

});



app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})