import React, { Component } from 'react';
import './style.css'
import { DataContext } from '../App.js';
import DisplayHomeItems from './displayHomeItems';

class Home extends Component {


    render() {
        return (
            <div>
                <DataContext.Consumer>
                    {(value) => (
                     <DisplayHomeItems value={value}></DisplayHomeItems>   
                    )
                    }
                </DataContext.Consumer>

            </div>
        )
    }
}

export default Home