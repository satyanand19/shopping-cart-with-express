import React, { Component, createContext } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';

import Navigation from './components/Navigation';
import RoutesOfpage from './components/Routers';
const DataContext = createContext();




class App extends Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
  }
  componentDidMount() {
    fetch('http://localhost:8000/')
      .then(response => response.json())
      .then(data => {
        this.setState({ data: data["response"]['data'] })
      });
  }
  handleIncrement = (index) => {

    let array = (this.state.data);
    array[index]['quantity'] += 1;
    this.setState({ data: array })
  }
  handleDecrement = (index) => {
    let array = this.state.data;
    array[index]['quantity'] -= 1;
    this.setState({ data: array })
  }
  render() {

    return (
      <div className="App">
        <BrowserRouter>
          <Navigation />
          <DataContext.Provider value={{
            data: this.state.data,
            handleDecrement: this.handleDecrement,
            handleIncrement: this.handleIncrement
          }}>
            <RoutesOfpage />
          </DataContext.Provider>
        </BrowserRouter>
      </div>
    );
  }
}
export default App;
export { DataContext };
